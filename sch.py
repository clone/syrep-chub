
import cgi, sys, os

repository_directory = "./rep"
syrep_binary = "/usr/local/bin/syrep"
header_done = 0

def print_header(s):
    global header_done
    print "Content-Type: text/html"
    print                          

    t = cgi.escape(s)
    print '<html><head><title>syrep-chub: %s</title><link rel="stylesheet" type="text/css" href="style.css"></head><body><h1>%s</h1>' % (t, t)
    header_done = 1

def print_footer():
    print '<div class="copyright">&copy; Lennart Poettering 2003</div>'
    print '</body></html>'

def error(s):
    global header_done

    if not header_done:
        print_header("Error");
    
    print '<div class="error">%s</div>' % cgi.escape(s)
    print_footer();
    sys.exit(0);

def run_proc(p):
    print '<pre>'
    fds = os.popen4(p, "r")

    for ln in fds[1]:
        print cgi.escape(ln),

    fds[0].close()
    fds[1].close()
        
    print '</pre>'

    
def valid_fname(fn):

    if fn.find("/") >= 0:
        return 0

    return 1
