#!/usr/bin/python

import cgi, cgitb, os, time, urllib, sch
cgitb.enable()

form = cgi.FieldStorage()

if not form.has_key("fname"):
    sch.error("No file name passed to script file")

fname = form["fname"].value

if not sch.valid_fname(fname):
    sch.error("Fuck off!")

sch.print_header("File Info for '%s'" % fname)

print '<p><a href="%s/%s">Download the file</a></p>' % (sch.repository_directory, fname)

sch.run_proc('%s --info "%s/%s" ' % (sch.syrep_binary, sch.repository_directory, fname))
sch.print_footer()
