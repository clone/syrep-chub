#!/usr/bin/python

import cgi, cgitb, os, time, urllib, sch
cgitb.enable()

form = cgi.FieldStorage()

if not form.has_key("fname"):
    sch.error("No file name passed to script file")

if not isinstance(form["fname"], list) or len(form["fname"]) != 2:
    sch.error("You are required to pass exactly two file names.")

fname = [form["fname"][0].value, form["fname"][1].value]
    
if not sch.valid_fname(fname[0]) or not sch.valid_fname(fname[1]):
    sch.error("Fuck off!")

sch.print_header("Differences between '%s' and '%s'" % (fname[0], fname[1]))
sch.run_proc('%s --diff "%s/%s" "%s/%s"' % (sch.syrep_binary, sch.repository_directory, fname[0], sch.repository_directory, fname[1]))
sch.print_footer()
