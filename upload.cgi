#!/usr/bin/python

import cgi, cgitb, os, time, urllib, sch, sys 

if os.getenv("REQUEST_METHOD") != "PUT":
    sch.error("Method PUT required.")

fname = os.getenv("QUERY_STRING")
if fname is None:
    sch.error("Need to pass query string.")

if os.getenv("CONTENT_LENGTH") is None:
    sch.error("Header field Content-Length missing.")
length = int(os.getenv("CONTENT_LENGTH"))

if not sch.valid_fname(fname):
    sch.error("Fuck off!")

sch.print_header("Upload for '%s'" % fname)

fname = sch.repository_directory + "/" + fname;
tfname = fname+".tmp"

out = file(tfname, "wb+")

total = 0

try:
    while 1:
        buf = sys.stdin.read(1024)

        if not len(buf):
            break

        out.write(buf)
        total += len(buf)
        
    out.close()
    
except IOError, e:
    os.unlink(tfname)
    sch.error("IOError: %s", str(e))

if total != length:
    os.unlink(tfname)
    sch.error("Incomplete upload.")

try:
    os.unlink(fname)
except OSError, e:
    pass

os.link(tfname, fname)
os.unlink(tfname)

print "<p>Successful</p>"

sch.print_footer()
