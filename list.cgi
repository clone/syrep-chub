#!/usr/bin/python

import cgi, cgitb, os, time, urllib, sch, stat
cgitb.enable()

def bsize(s):
    if s > 1024*1024:
        return "%i MB" % (s/1024/1024)
    elif s > 1024:
        return "%i KB" % (s/1024)
    else:
        return "%i B" % s

def list_files():
    d = os.listdir(sch.repository_directory)
    d.sort()

    for f in d:

        if f[0] == '.':
            continue
        
        st = os.stat(sch.repository_directory + "/" + f)

        if not stat.S_ISREG(st.st_mode):
            continue
        
        print '<tr><td class="checkbox"><input type="checkbox" name="fname" value="%s"></td>' % cgi.escape(f)
        print '<td class="filename"><a href="info.cgi?fname=%s">%s</a></td>' % (urllib.quote_plus(f), cgi.escape(f))
        print '<td class="filedate">%s</td>' % time.strftime("%c", time.localtime(st.st_mtime))
        print '<td class="filesize">%s</td></tr>' % bsize(st.st_size)


sch.print_header("File Listing")

print '<form action="diff.cgi" method="get"><table><tr><th></th><th>File name</th><th>Modification date</th><th>Size</th></tr>'

list_files()

print '<tr><td colspan="4" class="buttons">'

print '<input name="diff" type="submit" value="SYREP Diff"/>'

print '</td></tr></table></form>'

print '<p><a href="%s/">Browse the directory directly</a></p>' % sch.repository_directory

sch.print_footer()

